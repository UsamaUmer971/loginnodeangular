import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HeaderComponent } from 'src/components/header/header.component';
import { LoginComponent } from 'src/components/login/login.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResgisterComponent } from 'src/components/resgister/resgister.component';
import { HomeComponent } from 'src/components/home/home.component';
import { AdminComponent } from 'src/components/admin/admin.component';
import { ModeratorComponent } from 'src/components/moderator/moderator.component';
import { UserComponent } from 'src/components/user/user.component';
import { ProfileComponent } from 'src/components/profile/profile.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ResgisterComponent,
    HeaderComponent,
    HomeComponent,
    AdminComponent,
    ModeratorComponent,
    UserComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
