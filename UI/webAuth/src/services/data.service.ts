import { Injectable } from '@angular/core';
import { HttpClient ,HttpResponse ,HttpHeaders,HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { global_pointer } from "../assets/js/global_config";
// import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  ip :string=global_pointer.publicip;

  getAll(){
    return this.http.get(this.ip+'/all')
  }
  
}
