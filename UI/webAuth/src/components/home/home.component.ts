import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allData:any;

  constructor(private apiData: DataService, private http: HttpClient) { }

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.apiData.getAll().subscribe((data: any) =>{
      console.warn(this.allData=data)
    });
  }
}