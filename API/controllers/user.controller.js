exports.allAccess = (req, res) => {
    // res.status(200).send("Public Content.");
    return res.json("Public Content");
  };
  
  exports.userBoard = (req, res) => {
    // res.status(200).send("User Content.");
    return res.json("User Content");
  };
  
  exports.adminBoard = (req, res) => {
    // res.status(200).send("Admin Content.");
    return res.json("Admin Content");
  };
  
  exports.moderatorBoard = (req, res) => {
    // res.status(200).send("Moderator Content.");
    return res.json("Moderator Content");
    
  };